//
//  WeatherDetailsRouter.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 13/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit

class WeatherDetailsRouter: PresenterToRouterWeatherDetailsProtocol {
    var rootVc: UIViewController?
    
    init(rootVC: UIViewController) {
        self.rootVc = rootVC
    }
}
