//
//  WeatherDetailsContract.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 13/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit

//MARK: - View Input : View -> Presenter

protocol ViewToPresenterWeatherDetailsProtocol: class {
    var view: PresenterToViewWeatherDetailsProtocol? { get set }
    var interactor: PresenterToInteractorWeatherDetailsProtocol? { get set }
    var router: PresenterToRouterWeatherDetailsProtocol? { get set }

    func getCityWeatherData(cityName: String)
    func checkCityDataInDatabase(cityName: String)
    
    func getCurrentDate()
}

//MARK: - View Output : Presenter -> View

protocol PresenterToViewWeatherDetailsProtocol: class {
    func setCityWeatherDetailsOnSuccess(weatherInfo: WeatherInfo)
    func showErrorMessageOnFailure(error: Error)
}

//MARK: - Presenter Output : Presenter -> Interactor

protocol PresenterToInteractorWeatherDetailsProtocol: class {
    var presenter: InteractorToPresenterWeatherDetailsProtocol? { get set }
    
    var weatherInfo: WeatherInfo? { get set }
  
    func getWeatherByCityName(cityName: String, completionHandler: @escaping(WeatherInfo?, Error?) -> Void)
    
    func getCityDataFromDatabase(cityName: String, completionHandler: @escaping(Bool?, WeatherInfo?) -> Void)
}

//MARK: - Presenter Input : Interactor -> Presenter

protocol InteractorToPresenterWeatherDetailsProtocol: class {
    
}

//MARK: - Router input : View -> Router

protocol PresenterToRouterWeatherDetailsProtocol: class {
    var rootVc: UIViewController? { get set }
}
