//
//  WeatherDetailsViewController.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 13/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class WeatherDetailsViewController: UIViewController, PresenterToViewWeatherDetailsProtocol {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tempDetailsView: UIView!
    
    @IBOutlet weak var cityNameLabel: UILabel!
    
    @IBOutlet weak var weatherImage: UIImageView!
    
    @IBOutlet weak var weatherTitleLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var tempratureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var sunriseTimeLabel: UILabel!
    @IBOutlet weak var sunsetTimeLabel: UILabel!
    @IBOutlet weak var cloudCoverageLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    
    /// Presenter and router objects
    var presenter: ViewToPresenterWeatherDetailsProtocol?
    var router: PresenterToRouterWeatherDetailsProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        router = WeatherDetailsRouter(rootVC: self)
        presenter = WeatherDetailsPresenter(view: self, interactor: WeatherDetailsInteractor(), router: WeatherDetailsRouter(rootVC: self))
        self.setUpUI()
    }
    
    /// Set up UI :  Customise navigation bar and set search bar delegate
    func setUpUI() {
        configureNavigationBar(largeTitleColor: AppColor.NavigationBarColor.NavigationTitleColor, backgoundColor: AppColor.NavigationBarColor.NavigationBarBackgroundColor, tintColor: .white, title: "Weather Forcast", preferredLargeTitle: true)
        self.searchBar.delegate = self
        presenter?.getCurrentDate()
    }
    
    /// Set weather details on successfully fetching data from database or from Webservices
    /// - Parameter weatherInfo: weather info model object
    func setCityWeatherDetailsOnSuccess(weatherInfo: WeatherInfo) {
        ActivityIndicator.shared.stopLoading()
        self.scrollView.isHidden = false
        self.cityNameLabel.text = weatherInfo.cityName + ", " + weatherInfo.country
        self.dateLabel.text = Utilities.sharedInstance.convertDateToString(date: weatherInfo.date, dateFormat: AppConstants.DateFormat)
        self.weatherTitleLabel.text = weatherInfo.weatherTitle
        self.weatherDescriptionLabel.text = weatherInfo.weatherDescription
        self.tempratureLabel.text = String(format: "%.2f", weatherInfo.temprature) + UnitConstants.DegreeSymbol
        self.humidityLabel.text = String(format: "%.2f", weatherInfo.humadity) + UnitConstants.PercentageSymbol
        self.feelsLikeLabel.text = String(format: "%.2f", weatherInfo.feelsLike) + UnitConstants.DegreeSymbol
        self.sunriseTimeLabel.text = Utilities.sharedInstance.convertDateToString(date: weatherInfo.sunrise,dateFormat: AppConstants.TimeFormat)
        self.sunsetTimeLabel.text = Utilities.sharedInstance.convertDateToString(date: weatherInfo.sunset, dateFormat: AppConstants.TimeFormat)
        self.windSpeedLabel.text = String(format: "%.2f", weatherInfo.windSpeed) + UnitConstants.SpeedUnit
        self.windDirectionLabel.text = String(format: "%.2f", weatherInfo.windDirection) + UnitConstants.DegreeSymbol
        self.cloudCoverageLabel.text = String(format: "%.2f", weatherInfo.cloudCoverage) + UnitConstants.PercentageSymbol
        self.pressureLabel.text = String(format: "%.2f", weatherInfo.pressure) + UnitConstants.PressureUnit
        if let url = URL(string: APIConstants.weatherIconUrl + weatherInfo.weatherIcon + ".png"){
            self.weatherImage.af.setImage(withURL: url)
        }
    }
    
    /// Show error alert message if failed to fetch the data
    /// - Parameter error: error message to dispaly in alrt view controller
    func showErrorMessageOnFailure(error: Error) {
        ActivityIndicator.shared.stopLoading()
        Utilities.sharedInstance.genarateAlertViewWithTitle(title: AppConstants.APPNAME, message: error.localizedDescription, sender: self)
    }
}


extension WeatherDetailsViewController: UISearchBarDelegate {
    
    /// Search bar search button click action (Delegate method)
    /// - Parameter searchBar: search bar object
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.searchTextField.text != "" {
            ActivityIndicator.shared.showLoading(on: self)
            presenter?.checkCityDataInDatabase(cityName: searchBar.searchTextField.text!)
            self.view.endEditing(true)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
