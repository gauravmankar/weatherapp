//
//  WeatherDetailsInteractor.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 13/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation

class WeatherDetailsInteractor: PresenterToInteractorWeatherDetailsProtocol {
    
    var presenter: InteractorToPresenterWeatherDetailsProtocol?
    
    weak var presenterTest: WeatherDetailsPresenter?
    
    var weatherInfo: WeatherInfo?
    
    /// API call to fetch weather data of particular city name
    /// - Parameters:
    ///   - cityName: cityname
    ///   - completionHandler: which returns response data back to source from where its been called
    func getWeatherByCityName(cityName: String, completionHandler: @escaping(WeatherInfo?, Error?) -> Void) {
        APIHelper.sharedInstance.requestGETURL(APIConstants.Weather.getCityWeatherDataUrl(cityName: cityName), completionHandler: { (responseData) in
            do {
                let response = try JSONDecoder().decode(WeatherInfo.self, from: responseData.data!)
                completionHandler(response, nil)
            } catch {
              //  print(error)
                completionHandler(nil, error)
            }
        }) { (error) in
            print(error)
        }
    }
    
    /// Fetch the data from local database
    /// - Parameters:
    ///   - cityName: cityname
    ///   - completionHandler: Handles the response
    func getCityDataFromDatabase(cityName: String, completionHandler: @escaping (Bool?, WeatherInfo?) -> Void) {
        
        let weatherDataArray = CoreDataManager.shared().getWeatherDataFromDatabase(cityName: cityName)
        
        if weatherDataArray.count != 0 {
            completionHandler(true, WeatherInfo(data: weatherDataArray[0]))
        }else{
            completionHandler(false, nil)
        }
    }
}
