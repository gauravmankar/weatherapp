//
//  WeatherDetailsPresenter.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 13/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation

class WeatherDetailsPresenter: ViewToPresenterWeatherDetailsProtocol {
    
    var interactor: PresenterToInteractorWeatherDetailsProtocol?
    
    weak var view: PresenterToViewWeatherDetailsProtocol?
    
    var router: PresenterToRouterWeatherDetailsProtocol?
    
    /// Intitilise method
    /// - Parameters:
    ///   - view:presenter to view protocol object
    ///   - interactor: interact object
    ///   - router: router object
    init(view: PresenterToViewWeatherDetailsProtocol, interactor: WeatherDetailsInteractor, router: WeatherDetailsRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    /// Method to interact with interactor and fetch weather data based on city name passed by the user from search bar textfield
    /// - Parameter cityName: city name passed by user
    func getCityWeatherData(cityName: String) {
        interactor?.getWeatherByCityName(cityName: cityName, completionHandler: { (weatherInfo, error) in
            if error == nil {
                CoreDataManager.shared().saveWeatherDataToDatabase(dataForSaving: weatherInfo!)
                self.view?.setCityWeatherDetailsOnSuccess(weatherInfo: weatherInfo!)
            }else{
                self.view?.showErrorMessageOnFailure(error: error!)
            }
        })
    }
    
    /// Checks if search city data is already available in local database or not. If available then fetch tha data from local storage otherwise fetch the data from webservice
    /// - Parameter cityName: city name to be search
    func checkCityDataInDatabase(cityName: String) {
        interactor?.getCityDataFromDatabase(cityName: cityName, completionHandler: { (status, weatherInfo) in
            if status! {
              //  print("Success")
                CoreDataManager.shared().saveWeatherDataToDatabase(dataForSaving: weatherInfo!)
                self.view?.setCityWeatherDetailsOnSuccess(weatherInfo: weatherInfo!)
            }else{
                self.getCityWeatherData(cityName: cityName)
            }
        })
    }
    
    /// Compare current date with stored date if not equal then flushed the database
    func getCurrentDate() {
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        let str = df.string(from: Date())
        
        let userDefaults = UserDefaults.standard
        
        if let storedDate = userDefaults.value(forKey: AppConstants.DatabaseDate) {
            if storedDate as? String != str {
                self.clearWeatherDataFromDatabase()
                UserDefaults.standard.setValue(str, forKey: AppConstants.DatabaseDate)
            }
        }else{
            UserDefaults.standard.setValue(str, forKey: AppConstants.DatabaseDate)
        }
    }
    
    func clearWeatherDataFromDatabase() {
        CoreDataManager.shared().deleteAllRecords()
    }
}
