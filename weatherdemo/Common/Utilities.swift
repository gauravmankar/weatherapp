//
//  Utilities.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 15/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit

class Utilities {
    
    /// Singleton object of class
    
    static var sharedInstance = Utilities()
    
    private init(){
    }
    
    
    /// Function to  show alert view controller.
    /// - Parameters:
    ///   - title: Title of alert view
    ///   - message: Message to show on alert view
    ///   - sender: Object of View controller on which alert should be shown.
    
    func genarateAlertViewWithTitle(title: String, message: String, sender: UIViewController) -> Void{
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        sender.present(alert, animated: true, completion: nil)
    }
    
    /// Convert date from Seconds to string
    /// - Parameter date: Date in seconds (double return type)
    /// - Returns: Date/time in string format
    
    func convertDateToString(date: Double, dateFormat: String) -> String {
        let tempDate = NSDate(timeIntervalSince1970: date)
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = dateFormat
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        return formatter.string(from: tempDate as Date)
    }
}
