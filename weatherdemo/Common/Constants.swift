//
//  Constants.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 13/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit

/// Weather units format
enum WeatherUnits {
    case imperial
    case metric
}

/// App constants
struct AppConstants {
    static var APPNAME = "WeatherDemo"
    static var DatabaseDate = "storedDate"
    
    static var DateFormat = "EEE, dd MMM yyyy"
    static var TimeFormat = "h:mm a"
}

/// Weather units constants
struct UnitConstants {
    static var DegreeSymbol = " °C"
    static var PressureUnit = " hPa"
    static var SpeedUnit = " km/h"
    static var DistanceUnit = " km"
    static var PercentageSymbol = " %"
}

/// App color constants.
struct AppColor {
    struct NavigationBarColor {
        static var NavigationTitleColor = UIColor.white
        static var NavigationBarBackgroundColor = UIColor(red: 90.0/255.0, green: 200.0/255.0, blue: 250.0/255.0, alpha: 1)
    }
}
