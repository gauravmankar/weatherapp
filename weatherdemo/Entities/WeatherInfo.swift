//
//  WeatherInfo.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 13/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation

struct RawWeatherInfo: Codable {
    
    struct Coordinate: Codable {
        var lat: Float
        var lon: Float
    }

    struct WeatherData: Codable {
        var id: Int
        var main: String
        var description: String
        var icon: String
    }

    struct Main: Codable {
        var temp: Double
        var feels_like: Double
        var temp_min: Double
        var temp_max: Double
        var pressure: Double
        var humidity: Double
    }
    
    struct Sys: Codable {
        var country: String
        var sunrise: Double
        var sunset: Double
    }
    
    struct Wind: Codable {
        var speed: Double
        var deg: Double
    }
    
    struct Clouds: Codable {
        var all: Double
    }
    
    var id: Int
    var name: String
    var dt: Double
    var coord: Coordinate
    var weather: [WeatherData]
    var main: Main
    var sys: Sys
    var wind: Wind
    var clouds: Clouds
}

struct WeatherInfo: Codable {
    var id: String
    var latitude: Float
    var longitude: Float
    var date: Double
    var weatherId: Int
    var weatherTitle: String
    var weatherDescription: String
    var weatherIcon: String
    var temprature: Double
    var feelsLike: Double
    var tempMin: Double
    var tempMax: Double
    var pressure: Double
    var humadity: Double
    var cityName: String
    
    var country: String
    var sunset: Double
    var sunrise: Double
    
    var windSpeed: Double
    var windDirection: Double
    
    var cloudCoverage: Double
    
    init(from decoder: Decoder) throws {
        let rawResponse = try RawWeatherInfo(from: decoder)
        
        id = String(rawResponse.id)
        latitude = rawResponse.coord.lat
        longitude = rawResponse.coord.lon
        date = rawResponse.dt
        weatherId = rawResponse.weather.first!.id
        weatherTitle = rawResponse.weather.first!.main
        weatherDescription = rawResponse.weather.first!.description
        weatherIcon = rawResponse.weather.first!.icon
        temprature = rawResponse.main.temp
        feelsLike = rawResponse.main.feels_like
        tempMin = rawResponse.main.temp_min
        tempMax = rawResponse.main.temp_max
        pressure = rawResponse.main.pressure
        humadity = rawResponse.main.humidity
        cityName = String(rawResponse.name)
        country = rawResponse.sys.country
        sunset = rawResponse.sys.sunset
        sunrise = rawResponse.sys.sunrise
        windSpeed = rawResponse.wind.speed
        windDirection = rawResponse.wind.deg
        cloudCoverage = rawResponse.clouds.all
    }
    
    init(data: WeatherItem) {
        id = data.id
        latitude = data.latitude
        longitude = data.longitude
        date = data.date
        weatherId = Int(data.weatherId)
        weatherTitle = data.weatherTitle!
        weatherDescription = data.weatherDescription!
        weatherIcon = data.weatherIcon!
        temprature = data.temprature
        feelsLike = data.feelsLike
        tempMin = data.tempMin
        tempMax = data.tempMax
        pressure = data.pressure
        humadity = data.humadity
        cityName = data.cityName!
        country = data.country
        sunset = data.sunset
        sunrise = data.sunrise
        windSpeed = data.windSpeed
        windDirection = data.windDirection
        cloudCoverage = data.cloudCoverage
        date = data.date
    }
}
