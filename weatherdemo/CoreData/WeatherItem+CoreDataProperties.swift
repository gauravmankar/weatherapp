//
//  WeatherItem+CoreDataProperties.swift
//  
//
//  Created by Gaurav.Mankar on 15/10/20.
//
//

import Foundation
import CoreData

extension WeatherItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherItem> {
        return NSFetchRequest<WeatherItem>(entityName: "WeatherItem")
    }

    @NSManaged public var id: String
    @NSManaged public var latitude: Float
    @NSManaged public var longitude: Float
    @NSManaged public var weatherId: Int16
    @NSManaged public var weatherTitle: String?
    @NSManaged public var weatherDescription: String?
    @NSManaged public var weatherIcon: String?
    @NSManaged public var temprature: Double
    @NSManaged public var feelsLike: Double
    @NSManaged public var tempMin: Double
    @NSManaged public var tempMax: Double
    @NSManaged public var pressure: Double
    @NSManaged public var humadity: Double
    @NSManaged public var cityName: String?
    @NSManaged public var sunrise: Double
    @NSManaged public var sunset: Double
    @NSManaged public var country: String
    @NSManaged public var windSpeed: Double
    @NSManaged public var windDirection: Double
    @NSManaged public var cloudCoverage: Double
    @NSManaged public var date: Double
    
}
