//
//  CoreDataManager.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 15/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import CoreData


class CoreDataManager: NSObject {
    private override init() {
        super.init()
        applicationLibraryDirectory()
    }
    
    // Create a shared Instance
    static let _shared = CoreDataManager()
    
    // Shared Function
    class func shared() -> CoreDataManager{
        return _shared
    }
    
    
    
    
    /// Get the location where the core data DB is stored
    private lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
      //  print(urls[urls.count-1])
        return urls[urls.count-1]
    }()
    
    private func applicationLibraryDirectory() {
       // print(applicationDocumentsDirectory)
        if let url = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).last {
            print(url.absoluteString)
        }
    }
    
    
    // MARK: - Core Data stack

    // Get the managed Object Context
    lazy var managedObjectContext = {
        return self.persistentContainer.viewContext
    }()
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "weatherdemo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    /// Save data to database
    /// - Parameter dataForSaving: data which will be saving in database
    func saveWeatherDataToDatabase(dataForSaving: WeatherInfo) {
        let existedWeatherId = getAllPresentIds()
        //Add data to database after check on id present in DB or not
        
        if !(existedWeatherId.contains(dataForSaving.id)) {
            createWeatherItemEntity(from: dataForSaving)
        }
        self.saveContext()
    }
    
    
    /// This will create an entity for weather item
    /// - Parameter model: model which will use as value for entity.
    private func createWeatherItemEntity(from model: WeatherInfo) {
        let weatherItem = WeatherItem(context: self.managedObjectContext)
        weatherItem.id = model.id
        weatherItem.cityName = model.cityName
        weatherItem.latitude = model.latitude
        weatherItem.longitude = model.longitude
        weatherItem.feelsLike = model.feelsLike
        weatherItem.humadity = model.humadity
        weatherItem.pressure = model.pressure
        weatherItem.tempMax = model.tempMax
        weatherItem.tempMin = model.tempMin
        weatherItem.temprature = model.temprature
        weatherItem.weatherId = Int16(model.weatherId)
        weatherItem.weatherTitle = model.weatherTitle
        weatherItem.weatherDescription = model.weatherDescription
        weatherItem.weatherIcon = model.weatherIcon
        weatherItem.country = model.country
        weatherItem.sunrise = model.sunrise
        weatherItem.sunset = model.sunset
        weatherItem.cloudCoverage = model.cloudCoverage
        weatherItem.date = model.date
        weatherItem.windSpeed = model.windSpeed
        weatherItem.windDirection = model.windDirection
    }
    
    /// Description : Get weather data from database
    /// - Parameter cityName: city name of which data needs to be fetch from database
    /// - Returns: data in array format base on parameter cityname
    func getWeatherDataFromDatabase(cityName: String) -> [WeatherItem] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WeatherItem")

        if cityName != "" {
            fetchRequest.predicate = NSPredicate(format: "cityName = %@", cityName)
        }
        
        do{
            let results = try self.managedObjectContext.fetch(fetchRequest) as! [WeatherItem]
            return results
        } catch {
           // print(error)
        }
        
        return []
    }
    
    /// Check to fetch all existing ids
    /// - Returns: string array of ids from database
    private func getAllPresentIds()->[String]{
        return getWeatherDataFromDatabase(cityName: "").map{$0.id}
    }
    
    
    /// Deletes all data from database for entity name : WeatherItem
    func deleteAllRecords() {
        //getting context from your Core Data Manager Class
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "WeatherItem")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try managedObjectContext.execute(deleteRequest)
            try managedObjectContext.save()
        } catch {
            print ("There is an error in deleting records")
        }
    }
    
}
