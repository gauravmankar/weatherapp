//
//  APIConstants.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 13/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit

class APIConstants: NSObject {
    
    static let baseUrl = "https://api.openweathermap.org/data/2.5/"
    static let appIdItem = URLQueryItem(name: "appId", value: "d7a379e0adbb8404a0b37466279bd385")
    
    static let weatherIconUrl = "https://openweathermap.org/img/w/"
    
    /// Structure which make webservice urls
    struct Weather {
        static func getCityWeatherDataUrl(cityName: String) -> String {
            
            var components = URLComponents(string: baseUrl + "weather")
            
            let cityName = URLQueryItem(name: "q", value: cityName)
            let units = URLQueryItem(name: "units", value: "metric")
            
            components?.queryItems = [cityName, appIdItem, units]
            
            if let urlString = components?.url?.absoluteString {
                return urlString
            }else{
                return ""
            }
        }
        
        static func getWeatherIconUrl(iconId: String) -> String {
            let components = URLComponents(string: weatherIconUrl + iconId + ".png")
            if let urlString = components?.url?.absoluteString {
                return urlString
            }else{
                return ""
            }
        }
    }
}


