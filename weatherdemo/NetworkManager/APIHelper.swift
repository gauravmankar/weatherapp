//
//  APIHelper.swift
//  weatherdemo
//
//  Created by Gaurav.Mankar on 13/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

class APIHelper : NSObject {
    
    static var sharedInstance = APIHelper()
    
    private override init() {
    }
    
    func requestGETURL(_ strURL: String, completionHandler:@escaping (AFDataResponse<Any>) -> Void, failure:@escaping (AFError) -> Void) {
        AF.request(strURL).responseJSON { (responseObject) in
            if responseObject.error == nil {
                completionHandler(responseObject)
            }else{
                failure(responseObject.error!)
            }
        }
    }
}
