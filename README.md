# README #

This is Weather forcast app.
Using which user can search the weather details by city name.

App is using https://openweathermap.org/ to fetch weather details.
 
# Technology stack #

- Programming language : Swift 5.0
- Development tool : Xcode v11.6
- Architecture: VIPER
- Database : Sqlite(Coredata), Userdefaults

# Dependencies #

- "CodableAlamofire"
- "AlamofireImage"

# Functionality #

- User can search the city name to get weather details of that city.
- Home screen contain Search bar to search city name and weather details to display
- For first time the weather data is fetched from "https://openweathermap.org/" and it is stored in local database (Sqlite) using Coredata framework.
- If user is searching the same city for second time or more within the same day, then the weather data is fetched from database.
- Database is getting flushed when user is opening the app on next day.