//
//  testDateTimeFormat.swift
//  weatherdemoTests
//
//  Created by Gaurav.Mankar on 16/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import XCTest
@testable import weatherdemo

class testDateTimeFormat: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testDateFormat() {
        let dateChecker = Utilities.sharedInstance.convertDateToString(date: 1602829627, dateFormat: AppConstants.DateFormat)
        XCTAssertEqual(dateChecker, "Fri, 16 Oct 2020")
    }
    
    func testTimeFormat() {
        let timeChecker = Utilities.sharedInstance.convertDateToString(date: 1602809847, dateFormat: AppConstants.TimeFormat)
        XCTAssertEqual(timeChecker, "6:27 AM")
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
